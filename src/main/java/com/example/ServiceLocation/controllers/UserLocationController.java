package com.example.ServiceLocation.controllers;

import com.example.ServiceLocation.models.Location;
import com.example.ServiceLocation.models.User_Locations;
import com.example.ServiceLocation.repositories.UserLocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/userlocations")
public class UserLocationController {

    @Autowired
    UserLocationRepository userlocationRepository;

    @GetMapping
    public List<User_Locations> list() {
        return userlocationRepository.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    public User_Locations get(@PathVariable Long id) {
        if(userlocationRepository.findById(id).isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Location with ID "+id+" not found");
        }
        return userlocationRepository.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User_Locations create(@RequestBody final User_Locations user_locations) {
        return  userlocationRepository.saveAndFlush(user_locations);
    }
}
