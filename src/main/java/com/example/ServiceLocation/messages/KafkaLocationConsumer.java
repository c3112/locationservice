package com.example.ServiceLocation.messages;

import com.example.ServiceLocation.controllers.LocationsController;
import com.example.ServiceLocation.controllers.UserLocationController;
import com.example.ServiceLocation.models.KafkaUserLocation;
import com.example.ServiceLocation.models.Location;
import com.example.ServiceLocation.models.User_Locations;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class KafkaLocationConsumer {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    LocationsController locationsController = new LocationsController();

    @Autowired
    UserLocationController userLocationController = new UserLocationController();


    ArrayList<KafkaUserLocation> list_userlocation = new ArrayList<KafkaUserLocation>();

    List<KafkaUserLocation> location_userCovided = new ArrayList<KafkaUserLocation>();

    // KafkaAlertProducer producer = new KafkaAlertProducer(kafkaTemplate);

    Logger LOG = LoggerFactory.getLogger(KafkaLocationConsumer.class);
    @KafkaListener(topicPartitions =
            { @TopicPartition(topic = "topic-1",
                    partitionOffsets = @PartitionOffset(partition = "0", initialOffset = "0"))
            })
    public void listenerUserLocation(String data) throws ParseException
    {
        LOG.info(" [x] receive message " + data);
        //Gson gson = new Gson();
        Map usermap = jsonToMap(data);

        String username = (String) usermap.get("username");
        double latitude = (double) usermap.get("latitude");
        double longitude = (double) usermap.get("longitude");
        String timestampString = (String) usermap.get("timestamp");

        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy, h:mm:ss a");
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy, hh:mm:ss a", Locale.ENGLISH);
        Date date = sdf.parse(timestampString);
        KafkaUserLocation userlocation = new KafkaUserLocation(username,latitude,longitude,date);
        list_userlocation.add(userlocation);

        for(KafkaUserLocation usercovid : location_userCovided ){

            // Save positive person in Location
            Location locationPositif = new Location();
            locationPositif.setLocation_date(usercovid.getTimestamp());
            locationPositif.setLatitude(usercovid.getLatitude());
            locationPositif.setLongitude(usercovid.getLongitude());
            locationsController.create(locationPositif);

            // Save user location
            User_Locations user_locations = new User_Locations();
            user_locations.setUsername(username);
            user_locations.setLocation_id(locationPositif.getLocation_id());
            userLocationController.create(user_locations);

            // Sent message to user in proximity
            for(KafkaUserLocation userCompare : list_userlocation){
                System.out.println("------- start compare -----------");
                System.out.println(usercovid.getUsername());
                System.out.println(userCompare.getUsername());
                System.out.println(usercovid.getUsername()!= userCompare.getUsername());
                if(usercovid.getUsername()!= userCompare.getUsername()){
                    boolean resultCompare = usercovid.isProximity(userCompare);
                    if (resultCompare && userCompare.isAlerted() == false){
                        kafkaTemplate.send("topic-alert",userCompare.getJsonKafkaUserLocation());
                        LOG.info("[x] Send message " + userCompare.getJsonKafkaUserLocation());
                        userCompare.setAlerted(true);
                        //list_userlocation.remove(userCompare);
                    }
                }
            }
        }

    }

    @KafkaListener(topicPartitions =
            { @TopicPartition(topic = "topic-test",
                    partitionOffsets = @PartitionOffset(partition = "0", initialOffset = "0"))
            })
    public void listenerTest(String data) throws ParseException {
        LOG.info(data);
        Map testMap = jsonToMap(data);
        String username = (String) testMap.get("username");
        List<KafkaUserLocation> location_userFiltre = getUserLocationWithUsername(username);
        for(KafkaUserLocation userfiltre : location_userFiltre){
            location_userCovided.add(userfiltre);
        }

    }

    public static Map jsonToMap(String data){
        Gson gson = new Gson();
        Map dataMap = gson.fromJson(data, Map.class);
        return dataMap;
    }

    public List<KafkaUserLocation> getUserLocationWithUsername(String username){
        List<KafkaUserLocation> userList = list_userlocation.stream().filter(userlocation -> userlocation.getUsername().equals(username)).collect(Collectors.toList());
        return userList;
    }


}
