package com.example.ServiceLocation.models;

import com.google.gson.Gson;
import org.apache.kafka.common.protocol.types.Field;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class KafkaUserLocation {

    private String username;
    private double latitude;
    private double longitude;
    private Date timestamp;
    private boolean isAlerted;

    public KafkaUserLocation(String username, double latitude, double longitude, Date timestamp) {
        this.username = username;
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
        this.isAlerted = false;
    }

    public boolean isAlerted() {
        return isAlerted;
    }

    public void setAlerted(boolean alerted) {
        isAlerted = alerted;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isProximity(KafkaUserLocation user2){
        Boolean proximity = false;
        double distance = distanceBetweenUsers(user2);
        long timeDiff = timeDiff(user2);

        // Two user is proximity if distance < 30m and time difference in 20 min
        if(distance<(double)30 && timeDiff<(long)1200 && timeDiff>(long)0){
            proximity = true;
        }
        return proximity;
    }

    public double distanceBetweenUsers(KafkaUserLocation user2){
        double distance;

        double radlat1 = Math.toRadians(this.latitude);
        double radlat2 = Math.toRadians(user2.latitude);
        double positionR = Math.toRadians(this.latitude) - Math.toRadians(user2.latitude);
        double positionL = Math.toRadians(this.longitude) - Math.toRadians(user2.longitude);

        distance = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(positionR/2),2)+Math.cos(radlat1) * Math.cos(radlat2) * Math.pow(Math.sin(positionL/2),2)));

        //wgs84 calcul distance
        distance = distance * 6378137.0;
        distance= Math.round(distance * 10000) / 10000;
        System.out.println(" [info] distance between two user : " + distance);
        return distance;
    }

    public long timeDiff(KafkaUserLocation user2){
        long diff = user2.timestamp.getTime() - this.timestamp.getTime();
        long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
        System.out.println(" [info] time difference : " + seconds);
        return seconds;
    }

    public String getJsonKafkaUserLocation(){
        Map userlocation = new HashMap();
        userlocation.put("username",username);
        userlocation.put("latitude",latitude);
        userlocation.put("longitude",longitude);
        userlocation.put("timestamp",timestamp);
        Gson gson = new Gson();
        String json = gson.toJson(userlocation);
        return json;
    }
}
