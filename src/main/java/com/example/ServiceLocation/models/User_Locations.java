package com.example.ServiceLocation.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity(name="user_locations")
@Access(AccessType.FIELD)
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class User_Locations {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userlocation_id;
    private long location_id;
    private String username;

    public long getUserlocation_id() {
        return userlocation_id;
    }

    public void setUserlocation_id(long userlocation_id) {
        this.userlocation_id = userlocation_id;
    }

    public long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(long location_id) {
        this.location_id = location_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
