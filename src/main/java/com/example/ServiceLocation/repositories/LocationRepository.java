package com.example.ServiceLocation.repositories;

import com.example.ServiceLocation.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location,Integer> {

}
