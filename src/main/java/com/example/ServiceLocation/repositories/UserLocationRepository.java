package com.example.ServiceLocation.repositories;

import com.example.ServiceLocation.models.User_Locations;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserLocationRepository extends JpaRepository<User_Locations,Long> {
}
