package com.example.ServiceLocation;

import com.example.ServiceLocation.controllers.LocationsController;
import com.example.ServiceLocation.models.KafkaUserLocation;
import com.example.ServiceLocation.models.Location;
import com.github.database.rider.junit5.DBUnitExtension;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@ContextConfiguration(classes = ServiceLocationApplication.class)
@WebMvcTest(controllers = LocationsController.class)
@AutoConfigureMockMvc
class ServiceLocationApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private LocationsController locationsController;

	private String asJsonString(Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	void contextLoads() {
		assertThat(locationsController).isNotNull();
	}

	@Test
	@DisplayName("Test Get - Location")
	public void testGetLocation() throws Exception{
		mockMvc.perform(get("/api/v1/locations"))
				.andExpect(status().isOk());
	}

	@Test
	@DisplayName("Test Get - Location By ID")
	public void testGetLocationByIdFound() throws Exception{
		Location mockLocation = new Location();
		int id = 1;
		mockLocation.setLocation_id(id);
		mockLocation.setLatitude(43.6329382);
		mockLocation.setLongitude(3.8628207);

		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy, hh:mm:ss a", Locale.ENGLISH);
		Date date = sdf.parse("Nov 25, 2021, 12:32:45 PM");
		mockLocation.setLocation_date(date);

		doReturn(mockLocation).when(locationsController).getLocationByID(id);
		mockMvc.perform(get("/api/v1/locations/{id}",1))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.location_id",is(mockLocation.getLocation_id())))
				.andExpect(jsonPath("$.latitude", is(mockLocation.getLatitude())))
				.andExpect(jsonPath("$.longitude", is(mockLocation.getLongitude())))
				.andReturn().getResponse().getContentAsString();
	}

	@Test
	@DisplayName("Test Create Location")
	public void testControllerCreateLocation() throws Exception {
		Date date = new Date();
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);
		Location postLocation = new Location(43.6329382,3.8628207,ts);
		Location mockLocation = new Location(0,43.6329382,3.8628207,ts);
		//doReturn(mockLocation).when(locationsController).create(mockLocation);

		mockMvc.perform(post("/api/v1/locations")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(postLocation)))
                .andExpect(status().isCreated())
				.andReturn().getResponse().getContentAsString();
	}

	@Test
	@DisplayName("Test calculate distance")
	public void verifyDistance() {
		Date date = new Date();
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);
		KafkaUserLocation kafkaUser1 = new KafkaUserLocation("bob", 43.6329382,3.8628207,ts);
		KafkaUserLocation kafkaUser2 = new KafkaUserLocation("bob", 43.6371456,3.8535168,ts);
		double distance = kafkaUser2.distanceBetweenUsers(kafkaUser1);
		assertThat(distance).isEqualTo(883.0);
	}

	@Test
	@DisplayName("Test calculate time Diff")
	public void verifyTimeDiff(){
		Date date1 = new Date(2014,02,11,10,15,30);
		Date date2 = new Date(2014,02,11,10,15,40);
		KafkaUserLocation kafkaUser1 = new KafkaUserLocation("bob", 43.6329382,3.8628207,date1);
		KafkaUserLocation kafkaUser2 = new KafkaUserLocation("bob", 43.6371456,3.8535168,date2);
		double timediff = kafkaUser1.timeDiff(kafkaUser2);
		assertThat(timediff).isEqualTo(10.0);
	}

	public void verifyIsProximity(){
		Date date1 = new Date(2014,02,11,10,15,30);
		Date date2 = new Date(2014,02,11,10,15,40);
		KafkaUserLocation kafkaUser1 = new KafkaUserLocation("bob", 43.6329382,3.8628207,date1);
		KafkaUserLocation kafkaUser2 = new KafkaUserLocation("bob", 43.6371456,3.8535168,date2);
		boolean isProximity = kafkaUser1.isProximity(kafkaUser2);
		assertThat(isProximity).isEqualTo(false);
	}


}
